package com.example.lab3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
private List contacts;
Button btncontacts;
ListView lv;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
request();
onclick();


    }

    private void onclick() {
        btncontacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readAllContacts();
            }
        });
    }

    private void anhxa() {
    lv=findViewById(R.id.lvcontact);
    btncontacts=findViewById(R.id.btncontacts);
    }

    //retrieve all contacts
    private void readAllContacts(){
        Cursor cursor=getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,null,null,null);
        contacts=new ArrayList();
        int nameIndex=cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        while (cursor.moveToNext()){
          contacts.add(cursor.getString(nameIndex));
        }
        cursor.close();
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,contacts);
lv.setAdapter(arrayAdapter);
        Toast.makeText(this,"Size: "+contacts.size(),Toast.LENGTH_LONG).show();
    }
   private void request(){
       // Here, thisActivity is the current activity
       if (ContextCompat.checkSelfPermission(this,
               Manifest.permission.READ_CONTACTS)
               != PackageManager.PERMISSION_GRANTED) {

           // Permission is not granted
           // Should we show an explanation?
           if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                   Manifest.permission.READ_CONTACTS)) {
               // Show an explanation to the user *asynchronously* -- don't block
               // this thread waiting for the user's response! After the user
               // sees the explanation, try again to request the permission.
           } else {
               // No explanation needed; request the permission
               ActivityCompat.requestPermissions(this,
                       new String[]{Manifest.permission.READ_CONTACTS},
                       MY_PERMISSIONS_REQUEST_READ_CONTACTS);

               // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
               // app-defined int constant. The callback method gets the
               // result of the request.
           }
       } else {
           // Permission has already been granted
       }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
