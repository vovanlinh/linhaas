package com.example.lab3;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Bai3MediaPicture extends AppCompatActivity {
    private List media;
    Button btnmedia;
    GridView gv_folder;
    private static final int REQUEST_PERMISSIONS = 100;
    Uri Image_URI= MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai3_media_picture);
        request();
        anhxa();
onclick();

    }

    private void onclick() {
        btnmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // readAllCalllog();
                Intent intent=new Intent(Bai3MediaPicture.this,TextMedia.class);
                startActivity(intent);
            }
        });
    }

/*
    private void readAllCalllog(){
        Cursor cursor=getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null,null,null,null);
        media=new ArrayList();
        int nameIndex=cursor.getColumnIndex(MediaStore.Images.Media.DATA);
//int tenHinh=cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
        while (cursor.moveToNext()){
            media.add(cursor.getString(nameIndex));
        }
        cursor.close();
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,media);
        gv_folder.setAdapter(arrayAdapter);
        Toast.makeText(this,"Size: "+media.size(),Toast.LENGTH_LONG).show();
    }
    */
    private void anhxa() {
        gv_folder=findViewById(R.id.lvmedia);
        btnmedia=findViewById(R.id.btnmedia);
    }
    private void request(){
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(Bai3MediaPicture.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(Bai3MediaPicture.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                    ActivityCompat.requestPermissions(Bai3MediaPicture.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_PERMISSIONS);
            }
        }else {
            
        }
    }
  
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults.length > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        Toast.makeText(Bai3MediaPicture.this,
                                "The app was not allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    }
                }
            }
    }
}
}
