package com.example.lab3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.Browser;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Bai2BookmarkActivity extends AppCompatActivity {
    private List historycall;
    Button btnhiscall;
    ListView lvhiscall;
    private static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai2_bookmark);
request();
        anhxa();
        onclick();
    }
    private void onclick() {
        btnhiscall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllCallLog();
            }
        });
    }
/*
    private void readAllCalllog(){
        Cursor cursor=getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null,null,null,null);
        historycall=new ArrayList();
        int nameIndex=cursor.getColumnIndex(CallLog.Calls.NUMBER);
        //int nameDuration=cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        while (cursor.moveToNext()){
            historycall.add(cursor.getString(nameIndex));
        }
        cursor.close();
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,historycall);
        lvhiscall.setAdapter(arrayAdapter);
        Toast.makeText(this,"Size: "+historycall.size(),Toast.LENGTH_LONG).show();
    }
  */
private void getAllCallLog(){
    historycall=new ArrayList();


    Cursor cursor=getContentResolver().query(CallLog.Calls.CONTENT_URI,
           null,null,null,null);
    int number=cursor.getColumnIndex(CallLog.Calls.NUMBER);
    int type=cursor.getColumnIndex(CallLog.Calls.TYPE);
    int date=cursor.getColumnIndex(CallLog.Calls.DATE);
    int duration=cursor.getColumnIndex(CallLog.Calls.DURATION);
    while (cursor.moveToNext()){
        String phNumber=cursor.getString(number);
        String callType=cursor.getString(type);
        String callDate=cursor.getString(date);
        Date callDayTime=new Date(Long.valueOf(callDate));
        SimpleDateFormat format=new SimpleDateFormat("dd-MM-yy HH:mm");
        String datestring=format.format(callDayTime);
        String callDuration=cursor.getString(duration);
        String dir=null;
        int dirCode=Integer.parseInt(callType);
        switch (dirCode){
            case  CallLog.Calls.OUTGOING_TYPE:
                dir="OUTGOING ";
                break;
            case  CallLog.Calls.INCOMING_TYPE:
                dir="INCOMING ";
                break;
            case  CallLog.Calls.MISSED_TYPE:
                dir="MISSED ";
                break;
        }
        historycall.add("\nNumber Phone: "+phNumber+"\nCall Type: "+dir+"\nCall Date: "+
                datestring+"\nCall Duration: "+callDuration);
    }
    cursor.close();
    ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,historycall);
    lvhiscall.setAdapter(arrayAdapter);
    Toast.makeText(this,"Size: "+historycall.size(),Toast.LENGTH_LONG).show();
}
    private void anhxa() {
        lvhiscall=findViewById(R.id.lvcalllog);
        btnhiscall=findViewById(R.id.btncalllog);
    }
    private void request(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
