package com.example.lab3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GdActivity extends AppCompatActivity {
    Button btnb1,btnb2,btnb3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gd);
        anhxa();
        setonClick();
    }
    private void setonClick(){
        btnb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        btnb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,Bai2BookmarkActivity.class);
                startActivity(intent);
            }
        });
        btnb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,Bai3MediaPicture.class);
                startActivity(intent);
            }
        });
    }
    private void anhxa() {
        btnb1=findViewById(R.id.btnbai1);
        btnb2=findViewById(R.id.btnbai2);
        btnb3=findViewById(R.id.btnbai3);
    }
}
